package com.example.rent.myapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_1 = 1;
    private static final int REQUEST_CODE_2 = 2;
    private static final int REQUEST_CODE_3 = 3;

    private static final int REQUEST_LOAD_IMAGE_4 = 4;
    private static final int REQUEST_LOAD_IMAGE_5 = 5;
    private static final int REQUEST_LOAD_IMAGE_6 = 6;

    private static final int PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 7;
    private static final int PERMISSION_REQUEST_READ_EXTERNAL_STORAGE = 8;
    private static final int PERMISSION_REQUEST_CAMERA = 9;

    private static Bitmap photo;
    private ImageView imageViewTop;
    private ImageView imageViewBottom;
    private ImageView imageViewShoes;

    CustomAdapter customAdapter;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        customAdapter = new CustomAdapter(this);
        viewPager.setAdapter(customAdapter);

        imageViewTop = (ImageView) findViewById(R.id.imageViewTop);
        imageViewBottom = (ImageView) findViewById(R.id.imageViewBottom);
        imageViewShoes = (ImageView) findViewById(R.id.imageViewShoes);

    }

    @OnClick(R.id.imageViewTop)
    public void topPhoto(){
        intentToCamera(REQUEST_CODE_1);
    }

    @OnClick(R.id.imageViewBottom)
    public void bottomPhoto(){
        intentToCamera(REQUEST_CODE_2);
    }

    @OnClick(R.id.imageViewShoes)
    public void shoesPhoto(){
        intentToCamera(REQUEST_CODE_3);
    }

    @OnLongClick(R.id.imageViewTop)
    public boolean topPhotoLong(){
        intentToGallery(REQUEST_LOAD_IMAGE_4);
        return true;
    }

    @OnLongClick(R.id.imageViewBottom)
    public boolean bottomPhotoLong(){
        intentToGallery(REQUEST_LOAD_IMAGE_5);
        return true;
    }

    @OnLongClick(R.id.imageViewShoes)
    public boolean shoesPhotoLong(){
        intentToGallery(REQUEST_LOAD_IMAGE_6);
        return true;
    }

    private void intentToCamera(int requestIntentCamera) {
        Intent intentTop = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intentTop, requestIntentCamera);
    }

    private void intentToGallery(int requestLoadImage) {
        Intent intentTopLong = new Intent(Intent.ACTION_PICK);
        intentTopLong.setType("image/*");
        startActivityForResult(intentTopLong, requestLoadImage);
    }

//    private void cameryPermission(int requestLoadImage) {
//        if (android.support.v4.app.ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
//            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
//                    Manifest.permission.CAMERA)) {
//                // jesli dostep blokowal pokazujemy po co nam to potrzebne
//                showExplanation("Potrzebujemy pozwolenia", "Chcemy wysłać SMS który napisałeś, więc potrzebujemy pozwolenia",
//                        Manifest.permission.CAMERA, PERMISSION_REQUEST_CAMERA);
//            } else {
//                // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
//                requestPermissions(Manifest.permission.CAMERA, PERMISSION_REQUEST_CAMERA);
//            }
//        } else {
//            // z Edittext za pomoca funkcji getText().toString() wykonanej na kontrolce pobieramy wpisany tekst
//            // sendsms to wlasna funkcja do wysylania smsow opisana ponizej
//            intentToGallery(requestLoadImage);
//
//        }
//    }

//    private void showExplanation(String title, String message, final String permission, final int permissionRequestCode){
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle(title);
//        builder.setMessage(message);
//        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                requestPermissions(permission, permissionRequestCode);
//            }
//        });
//        builder.show();
//    }

    private void makePhoto(Intent data, ImageView imageView) {
        photo = (Bitmap) data.getExtras().get("data");
        Bundle extras = data.getExtras();
        photo = (Bitmap) extras.get("data");
        imageView.setAdjustViewBounds(true);
        imageView.setImageBitmap(photo);
        savePhotoToSDCard(photo);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){

            case 1:
                if(resultCode == RESULT_OK && requestCode == REQUEST_CODE_1) {
//                    checkPermission(data, imageViewTop);
                    makePhoto(data, imageViewTop);
                    toastMessage("Zapisano w galerii");
                }
                break;

            case 2:
                if(resultCode == RESULT_OK && requestCode == REQUEST_CODE_2) {
                    makePhoto(data, imageViewBottom);
                    toastMessage("Zapisano w galerii");
                }
                break;

            case 3:
                if(resultCode == RESULT_OK && requestCode == REQUEST_CODE_3) {
                    makePhoto(data, imageViewShoes);
                    toastMessage("Zapisano w galerii");
                }
                break;

            case 4:
                if(resultCode == RESULT_OK && requestCode == REQUEST_LOAD_IMAGE_4) {
                    getImageFromGallery(data, imageViewTop);
                }
                break;

            case 5:
                if(resultCode == RESULT_OK && requestCode == REQUEST_LOAD_IMAGE_5) {
                    getImageFromGallery(data,imageViewBottom);
                }
                break;

            case 6:
                if(resultCode == RESULT_OK && requestCode == REQUEST_LOAD_IMAGE_6) {
                    getImageFromGallery(data, imageViewShoes);
                }
                break;
        }
    }

    private void getImageFromGallery(Intent data, ImageView imageView) {
        Uri selectedImage = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
        cursor.moveToFirst();
        cursor.close();
        Bitmap bmp = null;
        try {
            bmp = getBitmapFromUri(selectedImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
        imageView.setImageBitmap(bmp);
    }

//    private void checkPermission(Intent data, ImageView imageView){
//        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
//        if(permission != PackageManager.PERMISSION_GRANTED){
//            ActivityCompat.requestPermissions(MainActivity.this, new String[] {Manifest.permission.CAMERA}, PERMISSION_REQUEST_CAMERA);
//        } else {
//            makePhoto(data, imageView);
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Dostęp przyznany", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Dostęp nie przyznany", Toast.LENGTH_LONG).show();
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE);
                        if (!showRationale) {
                            Toast.makeText(getApplicationContext(), "Użytkownik zaznaczył 'never ask again'", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                return;
//            case PERMISSION_REQUEST_READ_EXTERNAL_STORAGE:
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(getApplicationContext(), "Dostęp przyznany", Toast.LENGTH_LONG).show();
//                } else {
//                    Toast.makeText(getApplicationContext(), "Dostęp nie przyznany", Toast.LENGTH_LONG).show();
//                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
//                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
//                                Manifest.permission.READ_EXTERNAL_STORAGE);
//                        if (!showRationale) {
//                            Toast.makeText(getApplicationContext(), "Użytkownik zaznaczył 'never ask again'", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                }
//                return;
//            case PERMISSION_REQUEST_CAMERA:
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    Toast.makeText(getApplicationContext(), "Dostęp przyznany", Toast.LENGTH_LONG).show();
//                } else {
//                    Toast.makeText(getApplicationContext(), "Dostęp nie przyznany", Toast.LENGTH_LONG).show();
//                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
//                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
//                                Manifest.permission.CAMERA);
//                        if (!showRationale) {
//                            Toast.makeText(getApplicationContext(), "Użytkownik zaznaczył 'never ask again'", Toast.LENGTH_LONG).show();
//                        }
//                    }
//                }
//                return;
        }
    }


    private void toastMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }


//    private void requestForAccess(){
//        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
//        alertDialog.setTitle("Prośba o dostęp");
//        alertDialog.setMessage("Aplikacja potrzebuje dostępu do galerii aby załadować zdjęcie");
//        alertDialog.setPositiveButton("Zgadzam się", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                ActivityCompat.requestPermissions(MainActivity.this,
//                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                        PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
//            }
//        });
//        alertDialog.setNegativeButton("Nie zgadzam się", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        alertDialog.create();
//        alertDialog.show();
//    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    private void savePhotoToSDCard(Bitmap bitmap){
        File fileName;
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
            String photoName = simpleDateFormat.format(System.currentTimeMillis());
            String path = Environment.getExternalStorageDirectory().toString();

            new File(path + "/WhatToWear/").mkdirs();
            fileName = new File(path + "/WhatToWear/" + photoName + ".png");

            FileOutputStream fileOutputStream = new FileOutputStream(fileName);

            bitmap.compress(Bitmap.CompressFormat.PNG, 150, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            MediaStore.Images.Media.insertImage(getContentResolver(), fileName.getAbsolutePath(), fileName.getName(), fileName.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
